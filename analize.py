# -*- coding: utf-8 -*-


import pyttsx3
from actions import set_welcome, get_time, get_joke, get_weather, set_error


def analize_sentence(string):
	string = string.lower()
	if (string.find("hola") > -1) or (string.find("que tal") > -1):
		say_phrase(set_welcome())
	elif (string.find("chistes") > -1) or (string.find("bromas") > -1):
		say_phrase(get_joke())
	elif (string.find("hora") > -1) or (string.find("tiempo") > -1):
		say_phrase(get_time())
	elif (string.find("temperatura") > -1) or (string.find("clima") > -1) or (string.find("grados") > -1):
		say_phrase(get_weather())
	elif (string.find("error_microfono") > -1):
		say_phrase("Parece que hay un error. Favor de verificar el microfono.")
	else:
		say_phrase(set_error())
	pass


def say_phrase(string):
	engine = pyttsx3.init()
	engine.say(string)
	engine.runAndWait()
	pass

