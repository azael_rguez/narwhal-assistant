# -*- coding: utf-8 -*-


import os
import time
from pocketsphinx import LiveSpeech
from analize import analize_sentence


model_path = "models/"
while True:
	try:
		speech = LiveSpeech(
			verbose = False,
			sampling_rate = 16000,
			buffer_size = 2048,
			no_search = False,
			full_utt = False,
			hmm = os.path.join(model_path, 'es-es'),
			lm = os.path.join(model_path, 'es-20k.lm.bin'),
			dict = os.path.join(model_path, 'es.dict')
		)
		break
	except:
		analize_sentence("error_microfono")


if __name__ == "__main__":
	analize_sentence("hola")
	analize_sentence("temperatura")
	for phrase in speech:
		print(phrase)
		analize_sentence(str(phrase))
		time.sleep(5)
		pass
	pass
	
