# -*- coding: utf-8 -*-


import time
import datetime
import json
import random
import requests


def set_welcome():
	with open("statics/sentences.json", encoding="utf-8") as fh:
		data = json.load(fh)
		lenght = len(data["welcome"])-1
		return data["welcome"][random.randint(0, lenght)]
	pass


def get_time():
	st = datetime.datetime.fromtimestamp(time.time()).strftime('%H:%M')
	return "Son las {} horas.".format(st)
	pass


def get_joke():
	with open("statics/sentences.json", encoding="utf-8") as fh:
		data = json.load(fh)
		lenght = len(data["joke"])-1
		return data["joke"][random.randint(0, lenght)]
	pass


def get_weather():
	url = "http://api.openweathermap.org/data/2.5/weather?"
	key = "a053fbb0a63e2c695d13d83095e8ebc4"
	city = "Zapopan,mx"
	complete_url = url+"q="+city+"&APPID="+key
	try:
		response = requests.get(complete_url)
		data = response.json()
		temp = 0
		if data["cod"] is 200:
			temp = data["main"]["temp"]
			temp = float(temp)-273.15
			temp = int(temp)
			return "La temperatura actual de Zapopan es de aproximadamente {} grados.".format(temp)
		else:
			return "Una disculpa, hubo un error en la petición al servidor."
	except:
		return "Una disculpa, no tengo conexión a Internet."
	pass


def set_error():
	with open("statics/sentences.json", encoding="utf-8") as fh:
		data = json.load(fh)
		lenght = len(data["error"])-1
		return data["error"][random.randint(0, lenght)]
	pass